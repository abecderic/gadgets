/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Utils
{
    public static void setBlock(World world, int x, int y, int z, Material material)
    {
        setBlock(world, x, y, z, material, (byte)0);
    }

    public static void setBlock(World world, int x, int y, int z, Material material, byte data)
    {
        Block block = world.getBlockAt(x, y, z);
        block.setType(material);
        if (data != 0)
        {
            setBlockDataWrapper(block, data);
        }
    }

    /* no un-deprecated alternatives available and no proper documentation, too :( */
    @SuppressWarnings("deprecation")
    private static void setBlockDataWrapper(Block block, byte data)
    {
        block.setData(data);
    }
}
