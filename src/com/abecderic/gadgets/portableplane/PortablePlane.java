/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets.portableplane;

import com.abecderic.gadgets.GadgetsPlugin;
import com.abecderic.gadgets.IGadget;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PortablePlane implements IGadget, Listener
{
    private static final String NAME = "portableplane";
    private static final String DISPLAY_NAME = ChatColor.AQUA + "Tragbares Flugzeug";

    @Override
    public void onEnable(GadgetsPlugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void onDisable(GadgetsPlugin plugin)
    {
        /* NO-OP */
    }

    @Override
    public String getName()
    {
        return NAME;
    }

    @Override
    public ItemStack getItemStack()
    {
        ItemStack stack = new ItemStack(Material.EYE_OF_ENDER);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + DISPLAY_NAME);
        stack.setItemMeta(meta);
        return stack;
    }

    @Override
    public void onUse(Player player)
    {
        EnderPearl pearl = player.launchProjectile(EnderPearl.class);
        pearl.setPassenger(player);
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e)
    {
        if (e.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL)
        {
            e.setCancelled(true);
        }
    }
}
