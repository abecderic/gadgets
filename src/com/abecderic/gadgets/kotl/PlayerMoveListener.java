/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets.kotl;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener
{
    private KingOfTheLadderGadget kotl;

    public PlayerMoveListener(KingOfTheLadderGadget kotl)
    {
        this.kotl = kotl;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e)
    {
        for (Location location : kotl.getLadders())
        {
            if (e.getTo().getBlockX() == location.getBlockX() && e.getTo().getBlockZ() == location.getBlockZ())
            {
                if (e.getTo().getBlockY() == location.getBlockY() + KingOfTheLadderGadget.HEIGHT + 1)
                {
                    kotl.announceWin(location, e.getPlayer());
                    kotl.cleanUp(location);
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e)
    {
        if (e.getEntityType() == EntityType.PLAYER && e.getCause() == EntityDamageEvent.DamageCause.FALL)
        {
            e.setCancelled(true);
        }
    }
}
