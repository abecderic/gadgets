/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets.kotl;

import com.abecderic.gadgets.GadgetsPlugin;
import com.abecderic.gadgets.IGadget;
import com.abecderic.gadgets.Utils;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class KingOfTheLadderGadget implements IGadget
{
    private static final String NAME = "kotl";
    private static final String DISPLAY_NAME = ChatColor.LIGHT_PURPLE + "Ladder";
    static final int HEIGHT = 16;
    private static final int TOTAL_HEIGHT = HEIGHT + 3;

    private GadgetsPlugin plugin;
    private List<Location> ladders;

    @Override
    public void onEnable(GadgetsPlugin plugin)
    {
        this.plugin = plugin;

        plugin.getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), plugin);

        ladders = new ArrayList<>();
    }

    @Override
    public void onDisable(GadgetsPlugin plugin)
    {
        for (Location location : ladders)
        {
            placePole(location, Material.AIR, Material.AIR);
        }
    }

    @Override
    public String getName()
    {
        return NAME;
    }

    @Override
    public ItemStack getItemStack()
    {
        ItemStack stack = new ItemStack(Material.LADDER, 1);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + DISPLAY_NAME);
        stack.setItemMeta(meta);
        return stack;
    }

    @Override
    public void onUse(Player player)
    {
        Location location = player.getTargetBlock((Set<Material>) null, 3).getLocation().add(0, 1, 0);
        /* check for enough space */
        for (int xOffset = -1; xOffset <= 1; xOffset++)
        {
            for (int yOffset = 0; yOffset <= TOTAL_HEIGHT; yOffset++)
            {
                for (int zOffset = -1; zOffset <= 1; zOffset++)
                {
                    if (!location.getWorld().getBlockAt(location.getBlockX() + xOffset, location.getBlockY() + yOffset, location.getBlockZ() + zOffset).isEmpty())
                    {
                        player.sendMessage(ChatColor.RED + "Hier ist nicht genug Platz für King of the Ladder.");
                        return;
                    }
                }
            }
        }
        placePole(location, Material.BRICK, Material.LADDER);
        ladders.add(location);
    }

    void announceWin(Location location, Player player)
    {
        ladders.remove(location);
        Bukkit.broadcastMessage(ChatColor.AQUA + "Player " + player.getDisplayName() + ChatColor.AQUA + " won King of the Ladder!");
        for (Player other : location.getWorld().getPlayers())
        {
            /* check if player near pole and not winner */
            if (other.getLocation().getX() >= location.getBlockX() - 2 && other.getLocation().getX() < location.getBlockX() + 2
                    && other.getLocation().getZ() >= location.getBlockZ() - 2 && other.getLocation().getZ() < location.getBlockZ() + 2
                    && other.getLocation().getY() >= location.getBlockY() && other.getLocation().getY() < location.getBlockY() + TOTAL_HEIGHT
                    && other != player)
            {
                /* send player flying */
                player.setVelocity(player.getLocation().add(0.5, 0, 0.5).subtract(location).toVector().normalize());
            }
        }
        /* victory particles */
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(plugin, () ->
        {
            for (int i = 0; i < 64; i++)
            {
                double part = i / 64.0 * 2 * Math.PI;
                player.getWorld().playEffect(player.getLocation().add(Math.sin(part), 2.0, Math.cos(part)), Effect.COLOURED_DUST, 0);
            }
        }, 0, 1);
        /* cancel victory particles */
        Bukkit.getScheduler().runTaskLater(plugin, task::cancel, 64);
    }

    void cleanUp(Location location)
    {
        placePole(location, Material.AIR, Material.AIR);
    }

    private void placePole(Location location, Material mainMaterial, Material ladderMaterial)
    {
        for (int yOffset = 0; yOffset <= HEIGHT; yOffset++)
        {
            if (mainMaterial != Material.AIR)
            {
                Utils.setBlock(location.getWorld(), location.getBlockX(), location.getBlockY() + yOffset, location.getBlockZ(), mainMaterial);
            }

            Utils.setBlock(location.getWorld(), location.getBlockX() - 1, location.getBlockY() + yOffset, location.getBlockZ(), ladderMaterial, (byte)4);
            Utils.setBlock(location.getWorld(), location.getBlockX() + 1, location.getBlockY() + yOffset, location.getBlockZ(), ladderMaterial, (byte)5);
            Utils.setBlock(location.getWorld(), location.getBlockX(), location.getBlockY() + yOffset, location.getBlockZ() - 1, ladderMaterial, (byte)2);
            Utils.setBlock(location.getWorld(), location.getBlockX(), location.getBlockY() + yOffset, location.getBlockZ() + 1, ladderMaterial, (byte)3);

            if (mainMaterial == Material.AIR)
            {
                Utils.setBlock(location.getWorld(), location.getBlockX(), location.getBlockY() + yOffset, location.getBlockZ(), mainMaterial);
            }
        }
    }

    public List<Location> getLadders()
    {
        return ladders;
    }
}
