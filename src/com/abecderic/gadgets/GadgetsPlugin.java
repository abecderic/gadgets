/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class GadgetsPlugin extends JavaPlugin
{
    private static final String COMMAND = "gadgets";

    private Gadgets gadgets;

    @Override
    public void onEnable()
    {
        super.onEnable();
        gadgets = new Gadgets();
        getCommand(COMMAND).setExecutor(gadgets);
        getServer().getPluginManager().registerEvents(gadgets, this);
        gadgets.onEnable(this);
    }

    @Override
    public void onDisable()
    {
        super.onDisable();
        Bukkit.getScheduler().cancelAllTasks();
        gadgets.onDisable(this);
    }
}
