/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets.launcher;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.Map;

public class PlayerMoveListener implements Listener
{
    private static final double DISTANCE_SQUARED = 16;

    private LauncherGadget launcher;

    public PlayerMoveListener(LauncherGadget launcher)
    {
        this.launcher = launcher;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e)
    {
        /* if the player is on the ground, deny flight */
        if ((e.getPlayer().getGameMode() == GameMode.SURVIVAL || e.getPlayer().getGameMode() == GameMode.ADVENTURE)
            && !e.getPlayer().getLocation().subtract(0, 1, 0).getBlock().isEmpty())
        {
            e.getPlayer().setAllowFlight(false);
        }
        for (Map.Entry<Location, Vector> entry : launcher.getLaunchers().entrySet())
        {
            if (e.getTo().distanceSquared(entry.getKey()) <= DISTANCE_SQUARED)
            {
                if (e.getTo().getY() >= entry.getKey().getY())
                {
                    e.getPlayer().setVelocity(entry.getValue());
                    /* allow flight for the player so they don't get kicked by the Vanilla Anticheat */
                    e.getPlayer().setAllowFlight(true);
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e)
    {
        if (e.getEntityType() == EntityType.PLAYER && e.getCause() == EntityDamageEvent.DamageCause.FALL)
        {
            e.setCancelled(true);
        }
    }
}
