/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets.launcher;

import com.abecderic.gadgets.GadgetsPlugin;
import com.abecderic.gadgets.IGadget;
import com.abecderic.gadgets.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class LauncherGadget implements IGadget
{
    private static final String NAME = "launcher";
    private static final short CLAY_COLOR = 4;
    private static final String DISPLAY_NAME = ChatColor.YELLOW + "Launcher";
    private static final int BREAK_AFTER_TICKS = 5 * 60 * 20; // 5 min

    private GadgetsPlugin plugin;
    private Map<Location, Vector> launchers;

    @Override
    public void onEnable(GadgetsPlugin plugin)
    {
        this.plugin = plugin;

        plugin.getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), plugin);

        launchers = new HashMap<>();
    }

    @Override
    public void onDisable(GadgetsPlugin plugin)
    {
        for (Location location : launchers.keySet())
        {
            placeLauncher(location, Material.AIR, (byte) 0);
        }
    }

    @Override
    public String getName()
    {
        return NAME;
    }

    @Override
    public ItemStack getItemStack()
    {
        ItemStack stack = new ItemStack(Material.STAINED_CLAY, 1, CLAY_COLOR);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + DISPLAY_NAME);
        stack.setItemMeta(meta);
        return stack;
    }

    @Override
    public void onUse(Player player)
    {
        Location location = player.getLocation();
        /* check for enough space */
        for (int xOffset = -3; xOffset <= 3; xOffset++)
        {
            for (int yOffset = 0; yOffset <= 6; yOffset++)
            {
                for (int zOffset = -3; zOffset <= 3; zOffset++)
                {
                    if (!location.getWorld().getBlockAt(location.getBlockX() + xOffset, location.getBlockY() + yOffset, location.getBlockZ() + zOffset).isEmpty())
                    {
                        player.sendMessage(ChatColor.RED + "Hier ist nicht genug Platz für den Launcher.");
                        return;
                    }
                }
            }
        }
        /* place launcher */
        placeLauncher(location, Material.STAINED_CLAY, (byte) CLAY_COLOR);
        double xDirection = 0.0;
        double zDirection = 0.0;
        if (Math.abs(location.getDirection().getX()) > Math.abs(location.getDirection().getZ()))
        {
            xDirection = Math.signum(location.getDirection().getX());
        }
        else
        {
            zDirection = Math.signum(location.getDirection().getZ());
        }
        placeArrow(location, (int)xDirection, (int)zDirection);
        Vector vector = new Vector(xDirection, 0.25, zDirection).multiply(5.0);
        launchers.put(location, vector);
        Bukkit.getScheduler().runTaskLater(plugin, () ->
        {
           placeLauncher(location, Material.AIR, (byte) 0);
           launchers.remove(location);
        }, BREAK_AFTER_TICKS);

    }

    private void placeLauncher(Location location, Material material, byte data)
    {
        for (int xOffset = -3; xOffset <= 3; xOffset++)
        {
            for (int zOffset = -3; zOffset <= 3; zOffset++)
            {
                Utils.setBlock(location.getWorld(), location.getBlockX() + xOffset, location.getBlockY(), location.getBlockZ() + zOffset, material, data);
            }
        }
    }

    private void placeArrow(Location location, int xDirection, int zDirection)
    {
        Utils.setBlock(location.getWorld(), location.getBlockX() + xDirection, location.getBlockY(), location.getBlockZ() + zDirection, Material.COAL_BLOCK);
        Utils.setBlock(location.getWorld(), location.getBlockX() - zDirection, location.getBlockY(), location.getBlockZ() - xDirection, Material.COAL_BLOCK);
        Utils.setBlock(location.getWorld(), location.getBlockX() + zDirection, location.getBlockY(), location.getBlockZ() + xDirection, Material.COAL_BLOCK);
        Utils.setBlock(location.getWorld(), location.getBlockX() - zDirection * 2 - xDirection, location.getBlockY(), location.getBlockZ() - xDirection * 2 - zDirection, Material.COAL_BLOCK);
        Utils.setBlock(location.getWorld(), location.getBlockX() + zDirection * 2 - xDirection, location.getBlockY(), location.getBlockZ() + xDirection * 2 - zDirection, Material.COAL_BLOCK);
    }

    public Map<Location, Vector> getLaunchers()
    {
        return launchers;
    }
}
