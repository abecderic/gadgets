/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets;

import com.abecderic.gadgets.kotl.KingOfTheLadderGadget;
import com.abecderic.gadgets.launcher.LauncherGadget;
import com.abecderic.gadgets.portableplane.PortablePlane;
import com.google.gson.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Gadgets implements CommandExecutor, Listener
{
    private List<IGadget> gadgets;

    public void onEnable(GadgetsPlugin plugin)
    {
        List<IGadget> allGadgets = new ArrayList<>();
        allGadgets.add(new LauncherGadget());
        allGadgets.add(new KingOfTheLadderGadget());
        allGadgets.add(new PortablePlane());

        gadgets = new ArrayList<>();

        File config = new File(plugin.getDataFolder(), "config.json");
        if (config.exists())
        {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(config), StandardCharsets.UTF_8)))
            {
                JsonParser parser = new JsonParser();
                JsonElement rootElement = parser.parse(reader);
                JsonObject rootObject = rootElement.getAsJsonObject();
                for (IGadget gadget : allGadgets)
                {
                    if (rootObject.get(gadget.getName()).getAsBoolean())
                    {
                        gadgets.add(gadget);
                    }
                }
            }
            catch (JsonSyntaxException | JsonIOException | IOException e)
            {
                plugin.getLogger().log(Level.SEVERE, "Could not read configuration file.", e);
            }
        }
        else
        {
            plugin.getLogger().log(Level.WARNING, "Did not find configuration file, writing default file...");
            JsonObject json = new JsonObject();
            for (IGadget gadget : allGadgets)
            {
                json.addProperty(gadget.getName(), true);
                gadgets.add(gadget);
            }
            config.getParentFile().mkdirs();
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(config), StandardCharsets.UTF_8)))
            {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                writer.write(gson.toJson(json));
                writer.flush();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            plugin.getLogger().log(Level.WARNING, "Configuration file created at '" + config.getAbsolutePath() + "'.");
            plugin.getLogger().log(Level.WARNING, "Enabled all gadgets.");
        }

        for (IGadget gadget : gadgets)
        {
            gadget.onEnable(plugin);
        }
    }

    public void onDisable(GadgetsPlugin plugin)
    {
        for (IGadget gadget : gadgets)
        {
            gadget.onDisable(plugin);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e)
    {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if (e.getItem() != null)
            {
                for (IGadget gadget : gadgets)
                {
                    if (e.getItem().equals(gadget.getItemStack()))
                    {
                        e.setCancelled(true);
                        if (e.getPlayer().hasPermission("gadgets." + gadget.getName() + ".use"))
                        {
                            gadget.onUse(e.getPlayer());
                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings)
    {
        if (commandSender instanceof Player)
        {
            Player p = (Player) commandSender;
            if (p.hasPermission("gadgets.command"))
            {
                for (IGadget gadget : gadgets)
                {
                    p.getInventory().addItem(gadget.getItemStack());
                }
                commandSender.sendMessage(ChatColor.GREEN + "Alle " + gadgets.size() + " aktivierten Gadgets wurden dir ins Inventar gelegt.");
            }
            else
            {
                commandSender.sendMessage(ChatColor.RED + "Für diesen Command hast du nicht die erforderlichen Rechte.");
            }
        }
        else
        {
            commandSender.sendMessage(ChatColor.RED + "Dieser Command kann nur von einem Spieler ausgeführt werden.");
        }
        return true;
    }
}
