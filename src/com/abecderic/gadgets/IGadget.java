/* Copyright (C) 2018 by abecderic - All Rights Reserved */
package com.abecderic.gadgets;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface IGadget
{
    void onEnable(GadgetsPlugin plugin);

    void onDisable(GadgetsPlugin plugin);

    /**
     * @return the name of the gadget
     */
    String getName();

    /**
     * @return a copy of the itemstack that should trigger this gadget
     */
    ItemStack getItemStack();

    /**
     * Called when this gadget is triggered
     * @param player The player that triggered this gadget
     */
    void onUse(Player player);
}
