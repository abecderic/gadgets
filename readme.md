# Gadgets

## Overview

This plugin adds three new gadgets to your spigot server. All three gadgets can be gotten using the command '/gadgets'.
You'll have to integrate those items into your system.

## Installation

Drop the plugin into your plugins folder.

## Configuration

On first startup, the configuration file gets created; see the log for information on where the file exists.

The configuration file is a json file containing one boolean for every gadget which defines whether that gadget is
enabled.

## Gadgets

### Launcher ('launcher')

The launcher gadget, when used, creates a launch pad below the player. That pad launches any player stepping on it into
the direction the player faced when placing it. The launch direction is denoted by an arrow on the pad. The launch pad
self destructs after five minutes. When the plugin is disabled, the launch pad is also removed.

Note: This gadget disables fall damage.

### King of the Ladder ('kotl')

When used, this gadget places a 16 block high pole with ladders on all sides. The first player to stand on the top of
the pole, wins. When a player wins, the pole is removed, all other contestants are pushed to the side and the winner
lands gracefully\* in the middle. Their win is announced to all players and they get colorful victory particles for
a few seconds. When the plugin is disabled, the pole is also removed.

Note: This gadget disables fall damage. For this gadget to work, player versus player combat has to be enabled and
all players have to be in game mode survival.

\*: gracefulness not guaranteed

### Portable Plane ('portableplane')

When used, the player throws an ender pearl and gets put as a passenger onto that ender pearl, so they can ride it to
its destination. They can also get off the ender pearl whenever they want.

Note: This gadget disables regular ender pearl teleportation.

## Commands

(see plugin.yml file)

## Permissions

(see plugin.yml fle)